#!/usr/bin/python3 -I

# Author: 2024 Florian Haftmann <florian.haftmann@informatik.tu-muenchen.de>
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the
# Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more
# details.
#
# You should have received a copy of the GNU Affero General Public License along
# with this program. If not, see <https://www.gnu.org/licenses/>.


from typing import NoReturn, Optional
from collections.abc import Iterable, Iterator
import re
from pathlib import Path
import os
import stat
import json
import subprocess
import argparse
import argcomplete
import sys

from debian.changelog import Changelog
from debian.debian_support import Version


def message(s: str) -> None:

    print(s, file = sys.stderr)


def fail(s: str) -> NoReturn:

    message(s)
    raise SystemExit(1)


def is_ignored(p: Path) -> bool:

    if not Path('.git').is_dir():
        return False
    result = subprocess.run(['git', 'check-ignore', '-q', '--', p])
    return result.returncode == 0


def find_hashbang(hashbang_fragments: Iterable[str]) -> Iterator[Path]:

    re_hashbang = re.compile(r'#!.*\b(?:'
      + '|'.join(re.escape(hashbang_fragment) for hashbang_fragment in hashbang_fragments)
      + r')\b')
    for rel, _, names in os.walk(os.curdir):
        for name in names:
            p = Path(rel) / name
            if p.is_symlink():
                continue
            if p.stat().st_mode & stat.S_IXUSR != stat.S_IXUSR:
                continue
            first_line = p.read_text().splitlines()[0]
            if not re_hashbang.match(first_line):
                continue
            if is_ignored(p):
                continue
            yield p


def find_pys() -> Iterator[Path]:

    for p in Path().glob('**/*.py'):
        if is_ignored(p):
            continue
        yield p


def check_python_source() -> None:

    ps = [*find_pys(), *find_hashbang(['python', 'python3'])]
    if ps:
        subprocess.run(['flake8', '--show-source', '--config=setup.cfg', '.']
          + [str(p) for p in ps], check = True)
    for p in ps:
        subprocess.run(['mypy', '--strict', '--pretty', '--', str(p)], check = True)


def get_upstream_version(changelog: Changelog) -> str:

    changelog_version: Optional[Version] = changelog.get_version()
    if changelog_version is None:
        fail('Unable to retrieve version from debian/changelog')
    upstream_version: Optional[str] = changelog_version.upstream_version
    if upstream_version is None:
        fail('Unable to retrieve version from debian/changelog')
    return upstream_version


def check_metadata() -> None:

    version = json.loads(Path('manifest.json').read_text())['version']

    changelog = Changelog(Path('debian/changelog').read_text(), max_blocks = 1)

    if get_upstream_version(changelog) != version:
        fail(
            f'Version glitch in debian/changelog, expected »{version}«.\n'
            f'\n'
            f'For a snapshot brand, use\n'
            f'  gbp dch --new-version="{version}-1" --snapshot\n'
            f'\n'
            f'For a release brand, use\n'
            f'  gbp dch --new-version="{version}-1" --release\n'
            f'  git add …\n'
            f'  git commit …\n'
            f'  gbp buildpackage --git-tag\n'
        )


def check_copyright() -> None:

    subprocess.run(['reuse', 'lint'], check = True)


parser = argparse.ArgumentParser()
argcomplete.autocomplete(parser)
args = parser.parse_args()

os.chdir(Path(__file__).parent)

check_python_source()
check_metadata()
check_copyright()
